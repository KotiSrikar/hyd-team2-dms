package com.te.dms.service.implementation;

import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.te.dms.entity.AppUser;
import com.te.dms.entity.Roles;
import com.te.dms.exceptions.UserNotFoundException;
import com.te.dms.repository.AppUserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserDetailserviceImpl implements UserDetailsService {

	private final AppUserRepository appUserRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<AppUser> optionalAU = appUserRepository.findById(username);
		if (optionalAU.isPresent()) {
			Function<Roles, SimpleGrantedAuthority> function = e -> {
				return new SimpleGrantedAuthority(e.getRoleName());
			};
			Set<SimpleGrantedAuthority> authorities = optionalAU.get().getRoles().stream().map(function)
					.collect(Collectors.toSet());
			return new User(username,optionalAU.get().getPassword(), authorities);
		}
		throw new UserNotFoundException("Invalid credentials");
	}
}
