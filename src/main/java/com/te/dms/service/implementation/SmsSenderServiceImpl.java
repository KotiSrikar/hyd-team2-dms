package com.te.dms.service.implementation;

import org.springframework.stereotype.Service;

import com.te.dms.service.SmsSenderService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class SmsSenderServiceImpl implements SmsSenderService{

	public static final String ACCOUNT_SID="ACf72e785b8640318dc94737f8e50a0159";
	public static final String AUTH_TOKEN= "a28142353e1587f13d6d95b32f8407f1";
	
	@Override
	public void send(String message, Long phoneNo) {
		
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		Message info = Message.creator(new PhoneNumber("+91"+phoneNo),new PhoneNumber("19298224832"), message).create();
		System.out.println(info.getSid());
	}

}
