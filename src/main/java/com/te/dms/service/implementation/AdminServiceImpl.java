package com.te.dms.service.implementation;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.te.dms.dto.UsersDto;
import com.te.dms.entity.DmsUser;
import com.te.dms.repository.DmsUserRepository;
import com.te.dms.service.AdminService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AdminServiceImpl implements AdminService {

	private final DmsUserRepository dmsUserRepository;
 
	@Override
	public Optional<List<UsersDto>> getUsers() {
		List<DmsUser> usersList = dmsUserRepository.findAll();
		if (usersList.size() != 0) {
			Function<DmsUser, UsersDto> function = e -> {
				UsersDto userDto = new UsersDto();
				BeanUtils.copyProperties(e, userDto);
				return userDto;
			};
			List<UsersDto> usersDtoList = usersList.stream().map(function).collect(Collectors.toList());
			return Optional.ofNullable(usersDtoList);
		}
		return Optional.ofNullable(null);
	}

} 
