package com.te.dms.service;

import java.util.List;
import java.util.Optional;

import com.te.dms.dto.UsersDto;

public interface AdminService {

	Optional<List<UsersDto>> getUsers();

}
