package com.te.dms.service;

import java.io.IOException;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.te.dms.dto.ChangePassswordDto;
import com.te.dms.dto.DeleteDocumentDto;
import com.te.dms.dto.DeleteFileDto;
import com.te.dms.dto.DocumentDataDto;
import com.te.dms.dto.FetchProjectDto;
import com.te.dms.dto.FetchUserDto;
import com.te.dms.dto.FileRenameDto;
import com.te.dms.dto.ForgotPasswordDto;
import com.te.dms.dto.LoginDto;
import com.te.dms.dto.NewDocumentsDto;
import com.te.dms.dto.NewProjectDto;
import com.te.dms.dto.ShareFileDto;
import com.te.dms.dto.UserRegisterDto;
import com.te.dms.dto.UsersDto;

public interface DmsUserService {

	Optional<Boolean> register(UserRegisterDto userRegisterDto);

	Optional<Boolean> createProject(String userId, NewProjectDto newProjectDto);

	Optional<Integer> createNewDocument(String projectName, NewDocumentsDto newDocumentDto, String userId);

	Optional<String> uploadFile(String projectName, Integer documentId, MultipartFile file, String userId)
			throws IOException;

	Optional<DocumentDataDto> downloadFile(String fileName, String userId, String projectName, Integer documentId);

	Optional<FetchProjectDto> fetchData(String projectName, String userId);

	Boolean deleteFile(String userId, DeleteFileDto deleteFileDto);

	Boolean deleteDocument(String userId, DeleteDocumentDto deleteDocumentDto);

	Boolean deleteProject(String projectName, String userId);

	Boolean shareFiles(String userId, ShareFileDto sharefileDto);

	Optional<FetchUserDto> fetchUser(String userId);

	Optional<Boolean> updateUserInfo(String userId, UsersDto userDto);

	Optional<String> renameFile(String userId, FileRenameDto fileReNameDto);

	Optional<String> forgotPassword(ForgotPasswordDto forgotPasswordDto);

	Optional<Boolean> changePassword(String userId, ChangePassswordDto changePassswordDto);

}
