package com.te.dms.service;

public interface SmsSenderService {

	void send(String message, Long phoneNo);

}
