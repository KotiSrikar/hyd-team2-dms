package com.te.dms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ChangePassswordDto {
	
	private String oldPassword;
	
	private String newPassword;
	
	private String reEnterPassword;
	
	private String token;
}
