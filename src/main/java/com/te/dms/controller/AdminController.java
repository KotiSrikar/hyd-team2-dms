package com.te.dms.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.dms.dto.UsersDto;
import com.te.dms.exceptions.DataNotFoundException;
import com.te.dms.service.AdminService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "dms/admin")
public class AdminController {

	private final AdminService adminService;

	@GetMapping(path = "/userslist")
	public ResponseEntity<List<UsersDto>> fetchUsers() {
		Optional<List<UsersDto>> usersList = adminService.getUsers();
		if (usersList.isPresent()) {
			return ResponseEntity.ok(usersList.get());
		}
		throw new DataNotFoundException("no user found in the database");

	}
}
