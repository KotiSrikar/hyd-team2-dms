package com.te.dms.controller;

import java.util.Optional;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.dms.dto.ForgotPasswordDto;
import com.te.dms.dto.LoginDto;
import com.te.dms.dto.UserRegisterDto;
import com.te.dms.exceptions.CannotRegisterUserException;
import com.te.dms.exceptions.NewPasswordRequestDeclinedException;
import com.te.dms.response.GeneralResponse;
import com.te.dms.service.DmsUserService;
import com.te.dms.util.JwtUtils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/dms")
public class GeneralController {

	private final JwtUtils jwtUtils;
	private final DmsUserService dmsUserService;
	private final AuthenticationManager authenticationManager;

	@PostMapping(path = "/register")
	public GeneralResponse<String> userRegister(@RequestBody UserRegisterDto userRegisterDto) {
		Optional<Boolean> isRegistered = dmsUserService.register(userRegisterDto);
		if (isRegistered.get()) {
			return new GeneralResponse<String>("Registration Successfull", null, null);
		}
		throw new CannotRegisterUserException("unable to register the user please try again");

	}

	@GetMapping(path = "/login") 
	public GeneralResponse<String> login(@RequestBody LoginDto loginDto) {
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
		String token = jwtUtils.generateToken(loginDto.getUsername());
		return new GeneralResponse<String>("loggin succesfull", null, token);
	}

	@PostMapping(path = "/forgotpassword")
	public GeneralResponse<String> forgotPassword(@RequestBody ForgotPasswordDto forgotPasswordDto){
		Optional<String> forgotPassword = dmsUserService.forgotPassword(forgotPasswordDto);
		if(forgotPassword.isPresent()) {
			return new GeneralResponse<String>("New Password has been sent to your Registered emailId", forgotPassword.get(), null);
		}
		throw new NewPasswordRequestDeclinedException("New Password Request Declined");
	}
}
