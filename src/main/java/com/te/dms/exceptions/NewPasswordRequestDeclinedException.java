package com.te.dms.exceptions;

public class NewPasswordRequestDeclinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NewPasswordRequestDeclinedException(String message) {
		super(message);
	}
}
