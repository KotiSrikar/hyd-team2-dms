package com.te.dms.exceptions;

public class CannotFetchDataException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CannotFetchDataException(String message) {
		super(message);
	}

}
