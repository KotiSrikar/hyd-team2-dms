package com.te.dms.exceptions;

public class UnableToChangePasswordException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnableToChangePasswordException(String message) {
		super(message);
	}
}
