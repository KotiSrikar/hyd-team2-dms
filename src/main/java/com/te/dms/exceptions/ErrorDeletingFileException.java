package com.te.dms.exceptions;

public class ErrorDeletingFileException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorDeletingFileException(String message) {
		super(message);
	}

}
