package com.te.dms.exceptions;

public class CannotFetchUserException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CannotFetchUserException(String message) {
		super(message);
	}

}
