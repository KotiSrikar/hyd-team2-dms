package com.te.dms.exceptions;

public class FailedToRenameException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FailedToRenameException(String message) {
		super(message);
	}

}
