package com.te.dms.exceptions;

public class ErrorDeletingDocumentException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorDeletingDocumentException(String message) {
		super(message);
	}

}
