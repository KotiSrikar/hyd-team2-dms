package com.te.dms.exceptions;

public class CannotRenameFileException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CannotRenameFileException(String message) {
		super(message);
	}

}
