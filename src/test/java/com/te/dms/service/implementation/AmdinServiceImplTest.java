package com.te.dms.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.te.dms.dto.UsersDto;
import com.te.dms.entity.Admin;
import com.te.dms.entity.AppUser;
import com.te.dms.entity.DmsUser;
import com.te.dms.entity.Roles;
import com.te.dms.repository.AdminRepository;
import com.te.dms.repository.AppUserRepository;
import com.te.dms.repository.DmsUserRepository;
import com.te.dms.repository.DocumentDataRepository;
import com.te.dms.repository.DocumentFilesRepository;
import com.te.dms.repository.DocumentRepository;
import com.te.dms.repository.DocumentTypeRepository;
import com.te.dms.repository.ProjectsRepository;
import com.te.dms.repository.RolesRepository;
import com.te.dms.service.EmailSenderService;

@SpringBootTest
class AdminServiceImplTest {
 
	@Mock
	private AdminRepository adminRepository;

	@Mock
	private EmailSenderService emailSenderService;

	@Mock
	private AppUserRepository appUserRepository;

	@Mock
	private DmsUserRepository dmsUserRepository;

	@Mock
	private DocumentDataRepository documentDataRepository;

	@Mock
	private DocumentFilesRepository documentFilesRepository;

	@Mock
	private DocumentRepository documentRepository;

	@Mock
	private DocumentTypeRepository documentTypeRepository;

	@Mock
	private ProjectsRepository projectsRepository;

	@Mock
	private RolesRepository rolesRepository;

	@InjectMocks
	private AdminServiceImpl adminServiceImpl;

	@BeforeEach
	public void setUp() {
		Roles user = Roles.builder().roleName("ROLE_USER").build();
		Roles admin = Roles.builder().roleName("ROLE_ADMIN").appUsers(Lists.newArrayList()).build();

		Admin admin01 = Admin.builder().adminId("TE001").adminName("Srikar").build();

		AppUser adminCredentials = AppUser.builder().username(admin01.getAdminId()).password("qwerty")
				.roles(Lists.newArrayList()).build();

		admin.getAppUsers().add(adminCredentials);
		adminCredentials.getRoles().add(admin);

		rolesRepository.save(user);
		rolesRepository.save(admin);
		adminRepository.save(admin01);

		appUserRepository.save(adminCredentials);
	}

	@Test
	public void testGetUsers_ReturnsList() {
		List<DmsUser> user = new ArrayList<DmsUser>();
		DmsUser user1 = DmsUser.builder().emailId("yugandhar261@gmail.com").firstName("Rakesh reddy").lastName("s")
				.userId("Rakesh02").build();
		DmsUser user2 = DmsUser.builder().emailId("yugandhar262@gmail.com").firstName("Rakesh").lastName("s")
				.userId("Rakesh01").build();
		user.add(user2);
		user.add(user1);
		Mockito.when(dmsUserRepository.findAll()).thenReturn(user);
		Optional<List<UsersDto>> users = adminServiceImpl.getUsers();
		assertEquals(users.get().size(), 2);
	}

	@Test
	public void testGetUsers_ReturnsNull() {
		List<DmsUser> user = new ArrayList<DmsUser>();
		Mockito.when(dmsUserRepository.findAll()).thenReturn(user);
		assertEquals(Optional.ofNullable(null), adminServiceImpl.getUsers());
	}

}
