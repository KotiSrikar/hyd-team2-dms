package com.te.dms.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.te.dms.dto.ChangePassswordDto;
import com.te.dms.dto.DeleteDocumentDto;
import com.te.dms.dto.DeleteFileDto;
import com.te.dms.dto.DocumentDataDto;
import com.te.dms.dto.DocumentTypeDto;
import com.te.dms.dto.FetchDocumentsDto;
import com.te.dms.dto.FetchProjectDto;
import com.te.dms.dto.FetchUserDto;
import com.te.dms.dto.FileRenameDto;
import com.te.dms.dto.ForgotPasswordDto;
import com.te.dms.dto.NewDocumentsDto;
import com.te.dms.dto.NewProjectDto;
import com.te.dms.dto.ShareFileDto;
import com.te.dms.dto.UserRegisterDto;
import com.te.dms.dto.UsersDto;
import com.te.dms.entity.Admin;
import com.te.dms.entity.AppUser;
import com.te.dms.entity.DmsUser;
import com.te.dms.entity.DocumentData;
import com.te.dms.entity.DocumentFiles;
import com.te.dms.entity.DocumentType;
import com.te.dms.entity.Documents;
import com.te.dms.entity.Projects;
import com.te.dms.entity.Roles;
import com.te.dms.repository.AdminRepository;
import com.te.dms.repository.AppUserRepository;
import com.te.dms.repository.DmsUserRepository;
import com.te.dms.repository.DocumentDataRepository;
import com.te.dms.repository.DocumentFilesRepository;
import com.te.dms.repository.DocumentRepository;
import com.te.dms.repository.DocumentTypeRepository;
import com.te.dms.repository.ProjectsRepository;
import com.te.dms.repository.RolesRepository;
import com.te.dms.service.EmailSenderService;
import com.te.dms.service.SmsSenderService;
import com.te.dms.util.PasswordGenerator;

@SpringBootTest
class DmsUserServiceImplTest {

	@Mock
	private AdminRepository adminRepository;

	@Mock
	private EmailSenderService emailSenderService;

	@Mock
	private AppUserRepository appUserRepository;

	@Mock
	private DmsUserRepository dmsUserRepository;

	@Mock
	private DocumentDataRepository documentDataRepository;

	@Mock
	private DocumentFilesRepository documentFilesRepository;

	@Mock
	private DocumentRepository documentRepository;

	@Mock
	private DocumentTypeRepository documentTypeRepository;

	@Mock
	private ProjectsRepository projectsRepository;

	@Mock
	private RolesRepository rolesRepository;

	@Mock
	private PasswordEncoder passwordEncoder;

	@Mock
	private PasswordGenerator passwordGenerator;

	@Mock
	private SmsSenderService smsSenderService;
	
	@InjectMocks
	private DmsUserServiceImpl dmsUserServiceImpl;

	@BeforeEach
	public void setUp() {
		Roles user = Roles.builder().roleName("ROLE_USER").build();
		Roles admin = Roles.builder().roleName("ROLE_ADMIN").appUsers(Lists.newArrayList()).build();

		Admin admin01 = Admin.builder().adminId("TE001").adminName("Srikar").build();

		AppUser adminCredentials = AppUser.builder().username(admin01.getAdminId()).password("qwerty")
				.roles(Lists.newArrayList()).build();

		admin.getAppUsers().add(adminCredentials);
		adminCredentials.getRoles().add(admin);

		rolesRepository.save(user);
		rolesRepository.save(admin);
		adminRepository.save(admin01);

		appUserRepository.save(adminCredentials);
	}

	@Test
	public void testRegisterUser_ReturnTrue() {
		UserRegisterDto userRegisterDto = UserRegisterDto.builder().emailId("s@s.com").firstName("Srikar")
				.lastName("Koti").phoneNo(12345789L).password("qwerty").userId("TE001").build();
		DmsUser dmsUser = DmsUser.builder().emailId("s@s.com").firstName("Srikar").lastName("koti").phoneNo(123456789L)
				.projects(Lists.newArrayList()).userId("TE001").build();

		AppUser appUser = AppUser.builder().username(userRegisterDto.getUserId())
				.password(userRegisterDto.getPassword()).roles(Lists.newArrayList()).build();

		Roles roles = Roles.builder().roleName("ROLE_USER").appUsers(Lists.newArrayList()).build();
		appUser.getRoles().add(roles);
		roles.getAppUsers().add(appUser);
		Mockito.when(rolesRepository.findByRoleName(Mockito.any())).thenReturn(Optional.ofNullable(roles));
		Mockito.when(appUserRepository.save(Mockito.any())).thenReturn(appUser);
		Mockito.when(dmsUserRepository.save(Mockito.any())).thenReturn(dmsUser);
		Mockito.when(passwordEncoder.encode(Mockito.any())).thenReturn("qwerty");
		Optional<Boolean> register = dmsUserServiceImpl.register(userRegisterDto);
		assertTrue(register.get());

	}

	@Test
	public void testRegisterUser_ReturnFalse() {
		UserRegisterDto userRegisterDto = UserRegisterDto.builder().emailId("s@s.com").firstName("Srikar")
				.lastName("Koti").phoneNo(12345789L).password("qwerty").userId("TE001").build();
		DmsUser dmsUser = DmsUser.builder().emailId("s@s.com").firstName("Srikar").lastName("koti").phoneNo(123456789L)
				.projects(Lists.newArrayList()).userId("TE001").build();

		Mockito.when(dmsUserRepository.findById(userRegisterDto.getUserId())).thenReturn(Optional.ofNullable(null));

		AppUser appUser = AppUser.builder().username(userRegisterDto.getUserId())
				.password(userRegisterDto.getPassword()).roles(Lists.newArrayList()).build();

		Roles roles = Roles.builder().roleName("ROLE_USER").appUsers(Lists.newArrayList()).build();
		appUser.getRoles().add(roles);
		roles.getAppUsers().add(appUser);
		Mockito.when(rolesRepository.findByRoleName(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(appUserRepository.save(Mockito.any())).thenReturn(appUser);
		Mockito.when(dmsUserRepository.save(Mockito.any())).thenReturn(dmsUser);
		Mockito.when(passwordEncoder.encode(Mockito.any())).thenReturn("qwerty");
		Optional<Boolean> register = dmsUserServiceImpl.register(userRegisterDto);
		assertFalse(register.get());

	}

	@Test
	public void testCreateProject_ReturnsTrue() {
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		NewProjectDto newProjectDto = NewProjectDto.builder().contactName("Rakesh").projectId("Project-01")
				.projectName("DMS").build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Optional<Boolean> createProject = dmsUserServiceImpl.createProject("Rakesh01", newProjectDto);
		assertTrue(createProject.get());
	}

	@Test
	public void testCreateProject_ReturnFalse() {
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		NewProjectDto newProjectDto = NewProjectDto.builder().contactName("Rakesh").projectId("Project-01")
				.projectName("DMS").build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		Mockito.when(projectsRepository.findByProjectId(Mockito.any())).thenReturn(Optional.ofNullable(project));
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Optional<Boolean> createProject = dmsUserServiceImpl.createProject("Rakesh01", newProjectDto);
		assertFalse(createProject.get());
	}

	@Test
	public void testCreateNewDocument_ReturnsId() {
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").user(user)
				.build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		NewDocumentsDto newDocumentsDto = NewDocumentsDto.builder()
				.documentTypeDto(new DocumentTypeDto("Source", "source files")).build();
		Mockito.when(documentTypeRepository.findByName(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentRepository.save(Mockito.any())).thenReturn(document);
		Optional<Integer> document2 = dmsUserServiceImpl.createNewDocument("DMS", newDocumentsDto, "Rakesh01");
		assertEquals(Optional.ofNullable(document.getId()), document2);

	}

	@Test
	public void testCreateNewDocument_ReturnsNull() {
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").user(user)
				.build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		NewDocumentsDto newDocumentsDto = NewDocumentsDto.builder()
				.documentTypeDto(new DocumentTypeDto("Source", "source files")).build();
		Mockito.when(documentTypeRepository.findByName(Mockito.any()))
				.thenReturn(Optional.ofNullable(new DocumentType()));
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentRepository.save(Mockito.any())).thenReturn(document);
		Optional<Integer> document2 = dmsUserServiceImpl.createNewDocument("DMS", newDocumentsDto, "Rakesh01");
		assertEquals(Optional.ofNullable(null), document2);

	}

	@Test
	public void DeleteFile_ReturnTrue() {
		DeleteFileDto deleteFileDto = DeleteFileDto.builder().documentId(1).fileName("rakesh.pdf").projectName("ABC")
				.build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").user(user)
				.build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").documents(new Documents()).build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		Mockito.when(documentFilesRepository.findByFilename(Mockito.any())).thenReturn(Optional.ofNullable(file));
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Boolean deleteFile = dmsUserServiceImpl.deleteFile("Rakesh01", deleteFileDto);
		assertTrue(deleteFile);
	}

	@Test
	public void testDeleteFile_ReturnFalse() {
		DeleteFileDto deleteFileDto = DeleteFileDto.builder().documentId(1).fileName("rakesh.pdf").projectName("ABC")
				.build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").user(user)
				.build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").documents(new Documents()).build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Boolean deleteFile = dmsUserServiceImpl.deleteFile("Rakesh01", deleteFileDto);
		assertFalse(deleteFile);
	}

	@Test
	public void testdeleteDocument_ReturnTrue() {
		DeleteDocumentDto deleteDocumentDto = DeleteDocumentDto.builder().documentId(1).projectName("ABC").build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").user(user)
				.build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").documents(new Documents()).build();
		List<DocumentFiles> files = new ArrayList<DocumentFiles>();
		files.add(file);
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).documentFiles(files).build();
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentRepository.findByDocumentAndProject(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(document));
		Boolean isDeleted = dmsUserServiceImpl.deleteDocument("Rakesh01", deleteDocumentDto);
		assertTrue(isDeleted);
	}

	@Test
	public void testdeleteDocument_ReturnFalse() {
		DeleteDocumentDto deleteDocumentDto = DeleteDocumentDto.builder().documentId(1).projectName("ABC").build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").user(user)
				.build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(documentRepository.findByDocumentAndProject(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(document));
		Boolean isDeleted = dmsUserServiceImpl.deleteDocument("Rakesh01", deleteDocumentDto);
		assertFalse(isDeleted);
	}

	@Test
	public void testdeleteProject_ReturnsTrue() {

		List<Projects> projects = new ArrayList<Projects>();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.projects(projects).build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		assertTrue(dmsUserServiceImpl.deleteProject("DMS", "Rakesh01"));
	}

	@Test
	public void testdeleteProject_ReturnsFalse() {
		List<Projects> projects = new ArrayList<Projects>();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.projects(projects).build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		assertFalse(dmsUserServiceImpl.deleteProject("DMS", "Rakesh01"));
	}

	@Test
	public void testshareFiles_ReturnTrue() {
		ShareFileDto shareFileDto = ShareFileDto.builder().documentId(1).emailId("rakesh.s@gmail.com")
				.fileName("rakesh.pdf").projectName("DMS").build();
		List<Projects> projects = new ArrayList<Projects>();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.projects(projects).build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").documents(new Documents()).build();
		List<DocumentFiles> files = new ArrayList<DocumentFiles>();
		files.add(file);
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).documentFiles(files).build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Mockito.when(documentFilesRepository.findByFilename(Mockito.any())).thenReturn(Optional.ofNullable(file));
		assertTrue(dmsUserServiceImpl.shareFiles("Rakesh01", shareFileDto));

	}

	@Test
	public void testshareFiles_ReturnFalse() {
		ShareFileDto shareFileDto = ShareFileDto.builder().documentId(1).emailId("rakesh.s@gmail.com")
				.fileName("rakesh.pdf").projectName("DMS").build();
		List<Projects> projects = new ArrayList<Projects>();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.projects(projects).build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").documents(new Documents()).build();
		List<DocumentFiles> files = new ArrayList<DocumentFiles>();
		files.add(file);
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).documentFiles(files).build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Mockito.when(documentFilesRepository.findByFilename(Mockito.any())).thenReturn(Optional.ofNullable(file));
		assertFalse(dmsUserServiceImpl.shareFiles("Rakesh01", shareFileDto));

	}

	@Test
	public void testfetchUser_ReturnsDto() {
		List<Projects> projects = new ArrayList<Projects>();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		projects.add(project);
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.projects(projects).build();
		user.setProjects(projects);
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").build();
		List<DocumentFiles> files = new ArrayList<DocumentFiles>();
		files.add(file);
		List<Documents> documents = new ArrayList<Documents>();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).documentFiles(files).build();
		documents.add(document);
		project.setDocuments(documents);
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Optional<FetchUserDto> fetchUser = dmsUserServiceImpl.fetchUser("Rakesh01");
		assertEquals(fetchUser.get().getEmailId(), "s@s.com");
		assertEquals(1, fetchUser.get().getProjectDto().size());
		List<FetchProjectDto> projectDto = fetchUser.get().getProjectDto();
		assertEquals(projectDto.get(0).getDocumentsDto().size(), 1);
		List<FetchDocumentsDto> documentsDto = projectDto.get(0).getDocumentsDto();
		assertEquals(documentsDto.get(0).getDocumentFilesDto().size(), 1);

	}

	@Test
	public void testfetchUser_ReturnsNull() {
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		assertEquals(dmsUserServiceImpl.fetchUser("Rakesh01"), Optional.ofNullable(null));
	}

	@Test
	public void testfetchData_ReturnsData() {
		List<Projects> projects = new ArrayList<Projects>();
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		projects.add(project);
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").build();
		List<DocumentFiles> files = new ArrayList<DocumentFiles>();
		files.add(file);
		List<Documents> documents = new ArrayList<Documents>();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).documentFiles(files).build();
		documents.add(document);
		project.setDocuments(documents);
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentFilesRepository.findAllByDocuments(Mockito.any())).thenReturn(Optional.ofNullable(files));
		Optional<FetchProjectDto> fetchData = dmsUserServiceImpl.fetchData("DMS", "Rakesh01");
		assertEquals(1, fetchData.get().getDocumentsDto().size());
		List<FetchDocumentsDto> documentsDto2 = fetchData.get().getDocumentsDto();
		assertEquals(documentsDto2.get(0).getDocumentFilesDto().size(), 1);

	}

	@Test
	public void testfetchData_ReturnsNull() {
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));
		Mockito.when(documentFilesRepository.findAllByDocuments(Mockito.any())).thenReturn(Optional.ofNullable(null));
		assertEquals(dmsUserServiceImpl.fetchData("DMS", "Rakesh01"), Optional.ofNullable(null));
	}

	@Test
	public void testupdateUserInfo_ReturnsTrue() {
		UsersDto usersDto = UsersDto.builder().emailId("rakesh@gmail.com").firstName("Rakesh").lastName("Reddy")
				.phoneNo(6565165114l).build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Optional<Boolean> isUpdated = dmsUserServiceImpl.updateUserInfo("Rakesh01", usersDto);
		assertTrue(isUpdated.get());
	}

	@Test
	public void testupdateUserInfo_ReturnsFalse() {
		UsersDto usersDto = UsersDto.builder().emailId("rakesh@gmail.com").firstName("Rakesh").lastName("Reddy")
				.phoneNo(6565165114l).build();
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Optional<Boolean> isUpdated = dmsUserServiceImpl.updateUserInfo("Rakesh01", usersDto);
		assertFalse(isUpdated.get());
	}

	@Test
	public void testuploadFile_ReturnsPath() throws IOException {
		MockMultipartFile file = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.of(project));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Mockito.when(documentFilesRepository.findByFilename(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Optional<String> url = dmsUserServiceImpl.uploadFile("DMS", 1, file, "Rakesh01");
		String filepath = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/dms/users/" + "Rakesh01" + "/downloadfile/" + "DMS" + "/" + 1 + "/").path("rakesh.pdf")
				.toUriString();
		assertEquals(filepath, url.get());
	}

	@Test
	public void testuploadFile_ReturnsNull() throws IOException {

		MockMultipartFile file = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());
		DmsUser user = DmsUser.builder().emailId("s@s.com").firstName("Rakesh").lastName("s").userId("Rakesh01")
				.build();
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));
		assertEquals(dmsUserServiceImpl.uploadFile("DMS", 1, file, "Rakesh01"), Optional.ofNullable(null));

	}

	@Test
	public void testdownloadFile_DocumentData() {
		String key = "Rakesh01" + "DMS" + 1 + "rakesh.pdf";
		DocumentData documentData = DocumentData.builder().key1("Rakesh01DMS1rakesh.pdf").value("contenet".getBytes())
				.build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").build();
		DocumentDataDto dto = new DocumentDataDto("Rakesh01DMS1rakesh.pdf", "application/pdf", "contenet".getBytes());
		Mockito.when(documentDataRepository.findByKey1(Mockito.any())).thenReturn(Optional.ofNullable(documentData));
		Mockito.when(documentFilesRepository.findByFileName(Mockito.any())).thenReturn(Optional.ofNullable(file));
		Optional<DocumentDataDto> downloadFile = dmsUserServiceImpl.downloadFile("rakesh.pdf", "Rakesh01", "DMS", 1);
		assertEquals(key, downloadFile.get().getKey1());
		assertEquals("application/pdf", downloadFile.get().getFileType());
	}

	@Test
	public void testdownloadFile_ReturnsNull() {
		String key = "Rakesh01" + "DMS" + 1 + "rakesh.pdf";
		DocumentData documentData = DocumentData.builder().key1("Rakesh01DMS1rakesh.pdf").value("contenet".getBytes())
				.build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").build();
		DocumentDataDto dto = new DocumentDataDto("Rakesh01DMS1rakesh.pdf", "application/pdf", "contenet".getBytes());
		Mockito.when(documentDataRepository.findByKey1(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(documentFilesRepository.findByFileName(Mockito.any())).thenReturn(Optional.ofNullable(file));
		assertEquals(dmsUserServiceImpl.downloadFile("rakesh.pdf", "Rakesh01", "DMS", 1), Optional.ofNullable(null));

	}

	@Test
	public void testrenameFile_ReturnsUrl() {

		FileRenameDto renameDto = FileRenameDto.builder().documentId(1).existingName("rakesh.pdf").projectName("DMS")
				.newName("abc.pdf").build();

		String key = "Rakesh01" + "DMS" + 1 + "rakesh.pdf";
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		DocumentData documentData = DocumentData.builder().id(1).key1(key).value("contenet".getBytes()).build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").build();
		String filepath = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/dms/users/" + "Rakesh01" + "/downloadfile/" + "DMS" + "/" + 1 + "/").path("abc.pdf")
				.toUriString();
		Mockito.when(documentDataRepository.findByKey1(Mockito.any())).thenReturn(Optional.ofNullable(documentData));
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Mockito.when(documentFilesRepository.findByFileName(Mockito.any())).thenReturn(Optional.ofNullable(file));
		Optional<String> renameFile = dmsUserServiceImpl.renameFile("Rakesh01", renameDto);
		assertEquals(filepath, renameFile.get());
	}

	@Test
	public void testrenameFile_ReturnsNull() {

		FileRenameDto renameDto = FileRenameDto.builder().documentId(1).existingName("rakesh.pdf").projectName("DMS")
				.newName("abc.pdf").build();

		String key = "Rakesh01" + "DMS" + 1 + "rakesh.pdf";
		Projects project = Projects.builder().projectId("project1").projectName("DMS").contactName("Rakesh").build();
		DocumentData documentData = DocumentData.builder().id(1).key1(key).value("contenet".getBytes()).build();
		Documents document = Documents.builder().id(1).documentType(new DocumentType(1, "Source", "source files",
				LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null)).build();
		DocumentFiles file = DocumentFiles.builder().id(1).mime("application/pdf").fileName("rakesh.pdf")
				.filePath("http://localhost/downloadfile").build();
		String filepath = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/dms/users/" + "Rakesh01" + "/downloadfile/" + "DMS" + "/" + 1 + "/").path("abc.pdf")
				.toUriString();
		Mockito.when(documentDataRepository.findByKey1(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(projectsRepository.findByProjectandUser(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(project));
		Mockito.when(documentRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(document));
		Mockito.when(documentFilesRepository.findByFileName(Mockito.any())).thenReturn(Optional.ofNullable(file));

		assertEquals(dmsUserServiceImpl.renameFile("Rakesh01", renameDto), Optional.ofNullable(null));
	}

	@Test
	public void testforgotPassword_ReturnsEmailId() {
		ForgotPasswordDto forgotPasswordDto = ForgotPasswordDto.builder().emailId("yugandhar262@gmail.com")
				.userId("Rakesh01").build();
		AppUser appUser = AppUser.builder().username("Rakesh01").password("password").build();
		DmsUser user = DmsUser.builder().emailId("yugandhar262@gmail.com").firstName("Rakesh").lastName("s")
				.userId("Rakesh01").build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Mockito.when(appUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(appUser));
		Optional<String> email = dmsUserServiceImpl.forgotPassword(forgotPasswordDto);
		assertEquals(email.get(), user.getEmailId());
	}

	@Test
	public void testforgotPassword_ReturnsNull() {
		ForgotPasswordDto forgotPasswordDto = ForgotPasswordDto.builder().emailId("yugandhar262@gmail.com")
				.userId("Rakesh01").build();
		AppUser appUser = AppUser.builder().username("Rakesh01").password("password").build();
		DmsUser user = DmsUser.builder().emailId("yugandhar262@gmail.com").firstName("Rakesh").lastName("s")
				.userId("Rakesh01").build();
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Mockito.when(appUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));

		assertEquals(dmsUserServiceImpl.forgotPassword(forgotPasswordDto), Optional.ofNullable(null));
	}

	@Test
	public void testchangePassword_ReturnsTrue() {
		ChangePassswordDto changePassswordDto = ChangePassswordDto.builder().newPassword("rakesh")
				.oldPassword("password").reEnterPassword("rakesh").build();
		AppUser appUser = AppUser.builder().username("Rakesh01").password(passwordEncoder.encode("password")).build();
		DmsUser user = DmsUser.builder().emailId("yugandhar262@gmail.com").firstName("Rakesh").lastName("s")
				.userId("Rakesh01").build();
		Mockito.when(passwordEncoder.matches(Mockito.any(), Mockito.any())).thenReturn(true);
		Mockito.when(appUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(appUser));
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Optional<Boolean> isChanged = dmsUserServiceImpl.changePassword("Rakesh01", changePassswordDto);
		assertTrue(isChanged.get());
	}

	@Test
	public void testchangePassword_ReturnsFalse() {
		ChangePassswordDto changePassswordDto = ChangePassswordDto.builder().newPassword("rakesh")
				.oldPassword("password").reEnterPassword("rakesh").build();
		AppUser appUser = AppUser.builder().username("Rakesh01").password(passwordEncoder.encode("password")).build();
		DmsUser user = DmsUser.builder().emailId("yugandhar262@gmail.com").firstName("Rakesh").lastName("s")
				.userId("Rakesh01").build();
		Mockito.when(passwordEncoder.matches(Mockito.any(), Mockito.any())).thenReturn(false);
		Mockito.when(appUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(appUser));
		Mockito.when(dmsUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Optional<Boolean> isChanged = dmsUserServiceImpl.changePassword("Rakesh01", changePassswordDto);
		assertFalse(isChanged.get());
	}

}
