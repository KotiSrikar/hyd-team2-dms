package com.te.dms.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.te.dms.dto.UsersDto;
import com.te.dms.service.AdminService;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class AdminControllerTest {

	@Autowired 
	private MockMvc mockMvc;

	@MockBean
	private AdminService adminService;

	@InjectMocks
	private AdminController adminController;

	@Autowired
	private ObjectMapper objectMapper;

	
	@Test
	@WithMockUser(username ="admin01")
	public void testFetchUsers() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		List<UsersDto> usersDto = Lists.newArrayList();
		usersDto.add(
				UsersDto.builder().emailId("s@s.com").firstName("Srikar").lastName("koti").phoneNo(12345679L).build());
		usersDto.add(UsersDto.builder().emailId("s@s.com").lastName("sirigiri").firstName("Rakesh").phoneNo(123456789L)
				.build());
		Mockito.when(adminService.getUsers()).thenReturn(Optional.ofNullable(usersDto));
		mockMvc.perform(MockMvcRequestBuilders.get("/dms/admin/userslist").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(usersDto))).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	@WithMockUser(username = "admin01")
	public void testFetchUsers_BadRequest() throws JsonProcessingException, Exception {
		List<UsersDto> usersDto = Lists.newArrayList();
		usersDto.add(
				UsersDto.builder().emailId("s@s.com").firstName("Srikar").lastName("koti").phoneNo(12345679L).build());
		usersDto.add(UsersDto.builder().emailId("s@s.com").lastName("sirigiri").firstName("Rakesh").phoneNo(123456789L)
				.build());
		Mockito.when(adminService.getUsers()).thenReturn(Optional.ofNullable(null));
		mockMvc.perform(MockMvcRequestBuilders.get("/dms/admin/userslist").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(usersDto))).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
}
