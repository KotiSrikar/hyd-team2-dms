
package com.te.dms.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.te.dms.dto.ChangePassswordDto;
import com.te.dms.dto.DeleteDocumentDto;
import com.te.dms.dto.DeleteFileDto;
import com.te.dms.dto.DocumentDataDto;
import com.te.dms.dto.FetchProjectDto;
import com.te.dms.dto.FetchUserDto;
import com.te.dms.dto.FileRenameDto;
import com.te.dms.dto.NewDocumentsDto;
import com.te.dms.dto.NewProjectDto;
import com.te.dms.dto.ShareFileDto;
import com.te.dms.dto.UsersDto;
import com.te.dms.exceptions.CannotBeProcessedException;
import com.te.dms.exceptions.CannotFetchUserException;
import com.te.dms.exceptions.DocumentUploadFailedException;
import com.te.dms.exceptions.ErrorDeletingDocumentException;
import com.te.dms.exceptions.ErrorDeletingFileException;
import com.te.dms.exceptions.FailedToShareException;
import com.te.dms.exceptions.ProjectCannotbeCreatedException;
import com.te.dms.exceptions.UpdateFailedException;
import com.te.dms.response.GeneralResponse;
import com.te.dms.service.DmsUserService;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DmsUserService dmsUserService;
 
	@InjectMocks
	private UserController userController;

	ObjectMapper objectMapper = new ObjectMapper();

//Test Case For CreateProject..........................................	
	@Test
	@WithMockUser
	public void testCreateProject() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		NewProjectDto newProjectDto = NewProjectDto.builder().projectId("Ty001").projectName("DMS").contactName("Leo")
				.build();
		Mockito.when(dmsUserService.createProject(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/createproject")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(newProjectDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals(generalResponse.getMessage(), "A project has been created successfully");

	}

//Test Case For CreateProject which returns bad Http response as bad Request.....................
	@Test
	@WithMockUser
	public void testCreateProject_returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		NewProjectDto newProjectDto = NewProjectDto.builder().projectId("Ty001").projectName("DMS").contactName("Leo")
				.build();
		Mockito.when(dmsUserService.createProject(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(false));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/createproject")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(newProjectDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		ProjectCannotbeCreatedException readValue = objectMapper.readValue(contentAsString,
				ProjectCannotbeCreatedException.class);
		assertEquals("a new project cannot be created", readValue.getMessage());
	}

//Test Case For Delete Project ..............................................
	@Test
	@WithMockUser
	public void testDeleteProject() throws UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		Mockito.when(dmsUserService.deleteProject(Mockito.any(), Mockito.any())).thenReturn(true);
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/1/deleteproject/DMS")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals(generalResponse.getMessage(), "project has been deleted successfully");

	}

//Test Case For Delete Project which returns  response as Bad Request................
	@Test
	@WithMockUser
	public void testDeleteProject_returns400() throws UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		Mockito.when(dmsUserService.deleteProject(Mockito.any(), Mockito.any())).thenReturn(false);
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/1/deleteproject/DMS")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		CannotBeProcessedException readValue = objectMapper.readValue(contentAsString,
				CannotBeProcessedException.class);
		assertEquals("unable to delete project", readValue.getMessage());

	}

//Test Case For CreateDocument ..............................................
	@Test
	@WithMockUser
	public void testCreateDocument() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		NewDocumentsDto documentsDto = NewDocumentsDto.builder().documentTypeDto(null).build();

		Mockito.when(dmsUserService.createNewDocument(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(1));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/DMS/createdocument")
						.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(documentsDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals("Document has been created", generalResponse.getMessage());
	}

//Test Case For CreateDocument which returns bad Http response as bad Request.....................
	@Test
	@WithMockUser
	public void testCreateDocument_returns400()
			throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		NewDocumentsDto documentsDto = NewDocumentsDto.builder().documentTypeDto(null).build();

		Mockito.when(dmsUserService.createNewDocument(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/DMS/createdocument")
						.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(documentsDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		DocumentUploadFailedException readValue = objectMapper.readValue(contentAsString,
				DocumentUploadFailedException.class);
		assertEquals("unable to create document", readValue.getMessage());

	}

//Test case For DeleteDocument............................................................
	@Test
	@WithMockUser
	public void testDeleteDocument() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		DeleteDocumentDto deleteDocumentDto = DeleteDocumentDto.builder().documentId(1).build();
		Mockito.when(dmsUserService.deleteDocument(Mockito.any(), Mockito.any())).thenReturn(true);

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/1/deleteDocument").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(deleteDocumentDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals(generalResponse.getMessage(), "document has been deleted successfully");

	}

//Test Case For DeleteDocument which returns bad Http response as bad Request.....................
	@Test
	@WithMockUser
	public void testDeleteDocument_returns400()
			throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		DeleteDocumentDto deleteDocumentDto = DeleteDocumentDto.builder().documentId(1).build();
		Mockito.when(dmsUserService.deleteDocument(Mockito.any(), Mockito.any())).thenReturn(false);

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/1/deleteDocument").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(deleteDocumentDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		ErrorDeletingDocumentException readValue = objectMapper.readValue(contentAsString,
				ErrorDeletingDocumentException.class);
		assertEquals("unable to delete the document", readValue.getMessage());

	}

//Test Case For DeleteFile........................................................
	@Test
	@WithMockUser
	public void testDeleteFile() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		DeleteFileDto deleteFileDto = DeleteFileDto.builder().documentId(1).projectName("DMS").build();
		Mockito.when(dmsUserService.deleteFile(Mockito.any(), Mockito.any())).thenReturn(true);

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/1/deletefiles").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(deleteFileDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals(generalResponse.getMessage(), "file has been deleted successfully");

	}

//Test Case For DeleteFile which returns bad Http response as bad Request.....................
	@Test
	@WithMockUser
	public void testDeleteFile_returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		DeleteFileDto deleteFileDto = DeleteFileDto.builder().documentId(1).projectName("DMS").build();
		Mockito.when(dmsUserService.deleteFile(Mockito.any(), Mockito.any())).thenReturn(false);

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/1/deletefiles").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(deleteFileDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		ErrorDeletingFileException readValue = objectMapper.readValue(contentAsString,
				ErrorDeletingFileException.class);
		assertEquals("unable to delete the file ", readValue.getMessage());
	}

//Test Case For ShareFiles........................................................	
	@Test
	@WithMockUser
	public void testShareFiles() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		ShareFileDto shareFileDto = ShareFileDto.builder().fileName("leo").documentId(12).emailId("yuga@2324")
				.projectName("DMS").build();
		Mockito.when(dmsUserService.shareFiles(Mockito.any(), Mockito.any())).thenReturn(true);
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Tyoo1/sharefile")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(shareFileDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals("email has been shared", generalResponse.getMessage());

	}

//Test Case For ShareFiles which returns bad Http response as bad Request.....................
	@Test
	@WithMockUser
	public void testShareFiles_returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		ShareFileDto shareFileDto = ShareFileDto.builder().fileName("leo").documentId(12).emailId("yuga@2324")
				.projectName("DMS").build();
		Mockito.when(dmsUserService.shareFiles(Mockito.any(), Mockito.any())).thenReturn(false);
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/sharefile")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(shareFileDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		FailedToShareException readValue = objectMapper.readValue(contentAsString, FailedToShareException.class);
		assertEquals("unable to share the file via email", readValue.getMessage());

	}

//Test Case For UpdateUserInfo ..........................................	
	@Test
	@WithMockUser
	public void testUpdateUserInfo() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());

		UsersDto usersDto = UsersDto.builder().firstName("Yuga").lastName("Leo").emailId("Yuga@123").build();

		Mockito.when(dmsUserService.updateUserInfo(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/Ty001/updateinfo")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(usersDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals("user details has been updated", generalResponse.getMessage());

	}

//Test Case For UpdateUserInfo  which returns bad Http response as bad Request.............................	
	@Test
	@WithMockUser
	public void testUpdateUserInfo_returns400()
			throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());

		UsersDto usersDto = UsersDto.builder().firstName("Yuga").lastName("Leo").emailId("Yuga@123").build();

		Mockito.when(dmsUserService.updateUserInfo(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(false));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/Ty001/updateinfo")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(usersDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		UpdateFailedException readValue = objectMapper.readValue(contentAsString, UpdateFailedException.class);
		assertEquals(readValue.getMessage(), "failed to update the user details");

	}

//Test Case For FetchUser ...........................................................................	
	@Test
	@WithMockUser
	public void testFetchUser() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		FetchUserDto dto = FetchUserDto.builder().userId("1").build();
		Mockito.when(dmsUserService.fetchUser(Mockito.any())).thenReturn(Optional.ofNullable(dto));

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/dms/users/1/fetchUser")
						.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(dto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals(generalResponse.getMessage(), "user is Found for the User id  " + dto.getUserId());

	}

//Test Case For FetchUser  which returns bad Http response as bad Request.............................		
	@Test
	@WithMockUser
	public void testFetchUser_returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		FetchUserDto dto = new FetchUserDto();
		Mockito.when(dmsUserService.fetchUser(Mockito.any())).thenReturn(Optional.ofNullable(null));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/dms/users/1/fetchUser")
						.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(dto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		CannotFetchUserException readValue = objectMapper.readValue(contentAsString, CannotFetchUserException.class);
		assertEquals(readValue.getMessage(), " User is Not Found ");
	}

//Test Case For FetchData ...........................................................................	
	@Test
	@WithMockUser
	public void testFetchData() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());

		FetchProjectDto fetchProjectDto = FetchProjectDto.builder().projectId("12").projectName("DMS").build();
		Mockito.when(dmsUserService.fetchData(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(fetchProjectDto));

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/dms/users/1/DMS/getData")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(fetchProjectDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals(generalResponse.getMessage(), fetchProjectDto.getProjectName());
	}

//Test case for fetchData with bad request ...................................................................
	@Test
	@WithMockUser
	public void testFetchData_BadRequest() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		FetchProjectDto fetchProjectDto = FetchProjectDto.builder().projectId("12").projectName("DMS").build();
		Mockito.when(dmsUserService.fetchData(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(null));
		mockMvc.perform(
				MockMvcRequestBuilders.get("/dms/users/1/DMS/getData").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(fetchProjectDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

//Test case for upload file ................................................................................

	@Test
	@WithMockUser
	public void testUploadFile() throws Exception {
		objectMapper.registerModule(new JavaTimeModule());
		MockMultipartFile file = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());

		Mockito.when(dmsUserService.uploadFile(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable("abc"));

		String result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/dms/users/TE001/dms/1/addfile").file(file)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(result, GeneralResponse.class);
		assertEquals("document has been uploaded", generalResponse.getMessage());

	}

//Test case to upload Multiple files.................................................................................
	
	@Test
	@WithMockUser
	public void testUploadFiles() throws Exception {
		objectMapper.registerModule(new JavaTimeModule());

		MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());
		MockMultipartFile mockMultipartFile2 = new MockMultipartFile("file", "srikar.pdf", "application/pdf",
				"This is the content".getBytes());
		Mockito.when(dmsUserService.uploadFile(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable("abc"));
		String result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/dms/users/TE001/dms/1/addfiles").file(mockMultipartFile)
						.file(mockMultipartFile2).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(result, GeneralResponse.class);
	}

// Test case for Upload file bad request ..................................................................

	@Test
	@WithMockUser
	public void testUploadFile_BadRequest() throws Exception {
		objectMapper.registerModule(new JavaTimeModule());
		MockMultipartFile file = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());

		Mockito.when(dmsUserService.uploadFile(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));

		String result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/dms/users/TE001/dms/1/addfile").file(file)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(result, GeneralResponse.class);
		assertEquals("failed to upload file", generalResponse.getMessage());
	}

//Test case for Upload Multiple files bad Request ..............................................................
	
	@Test
	@WithMockUser
	public void testUploadFiles_badRequest() throws Exception {
		objectMapper.registerModule(new JavaTimeModule());

		MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());
		MockMultipartFile mockMultipartFile2 = new MockMultipartFile("file", "srikar.pdf", "application/pdf",
				"This is the content".getBytes());
		Mockito.when(dmsUserService.uploadFile(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));
		mockMvc.perform(MockMvcRequestBuilders.multipart("/dms/users/TE001/dms/1/addfiles").file(mockMultipartFile)
				.file(mockMultipartFile2).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());

	}

//Test case for Download file ..............................................................................

	@Test
	@WithMockUser
	public void testDownloadFile() throws Exception {
		objectMapper.registerModule(new JavaTimeModule());
		MockMultipartFile file = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());

		DocumentDataDto documentDataDto = DocumentDataDto.builder().key1("abc").fileType("application/txt")
				.value("Srikar".getBytes()).build();

		Mockito.when(dmsUserService.downloadFile(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(documentDataDto));

		String result = mockMvc
				.perform(MockMvcRequestBuilders.get("/dms/users/TE001/downloadfile/dms/1/file")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

	}

//Test case for Download File with bad Request ............................................................

	@Test
	@WithMockUser
	public void testDownloadFile_badRequest() throws Exception {
		objectMapper.registerModule(new JavaTimeModule());
		MockMultipartFile file = new MockMultipartFile("file", "rakesh.pdf", "application/txt",
				"This is the content".getBytes());

		DocumentDataDto documentDataDto = DocumentDataDto.builder().key1("abc").fileType("application/txt")
				.value("Srikar".getBytes()).build();

		Mockito.when(dmsUserService.downloadFile(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(null));

		String result = mockMvc
				.perform(MockMvcRequestBuilders.get("/dms/users/TE001/downloadfile/dms/1/file")
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

//TEst case for change password ...........................................................................

	@Test
	@WithMockUser
	public void testChangePassword() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		ChangePassswordDto changePassswordDto = ChangePassswordDto.builder().oldPassword("qwerty").newPassword("ytrewq")
				.reEnterPassword("ytrewq").build();

		Mockito.when(dmsUserService.changePassword(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/changepassword")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(changePassswordDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals("Your Password is changed Successfully", generalResponse.getMessage());
	}

//Test case for change password with bad request ...............................................................

	@Test
	@WithMockUser
	public void testChangePassword_badRequest()
			throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
//		ChangePassswordDto changePassswordDto = ChangePassswordDto.builder().oldPassword("qwerty").newPassword("ytrewq")
//				.reEnterPassword("ytrewq").build();
		ChangePassswordDto changePassswordDto = new ChangePassswordDto();

		Mockito.when(dmsUserService.changePassword(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(false));
		mockMvc.perform(MockMvcRequestBuilders.post("/dms/users/Ty001/changepassword")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(changePassswordDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

//Test case for Rename file ..................................................................................

	@Test
	@WithMockUser
	public void testRenameFile() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		FileRenameDto fileRenameDto = FileRenameDto.builder().existingName("file").newName("file2")
				.projectName("project-01").build();
		String filepath = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/dms/users/" + "Rakesh01" + "/downloadfile/" + "DMS" + "/" + 1 + "/").path("abc.pdf")
				.toUriString();
		Mockito.when(dmsUserService.renameFile(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(filepath));

		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/dms/users/Ty001/renameFile")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(fileRenameDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		GeneralResponse<String> generalResponse = objectMapper.readValue(contentAsString, GeneralResponse.class);
		assertEquals("Rename succesfull", generalResponse.getMessage());

	}

//Test case  for Rename file with bad request ................................................................

	@Test
	@WithMockUser
	public void testRenameFile_BadRequest() throws JsonProcessingException, Exception {
		objectMapper.registerModule(new JavaTimeModule());
		FileRenameDto fileRenameDto = FileRenameDto.builder().existingName("file").newName("file2")
				.projectName("project-01").build();
		String filepath = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/dms/users/" + "Rakesh01" + "/downloadfile/" + "DMS" + "/" + 1 + "/").path("abc.pdf")
				.toUriString();
		Mockito.when(dmsUserService.renameFile(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(null));
		mockMvc.perform(MockMvcRequestBuilders.put("/dms/users/Ty001/renameFile")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(fileRenameDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

}
